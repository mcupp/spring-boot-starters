# Upgrade from Spring Boot 2.6 to 2.7

## Docs
[Release Notes](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.7-Release-Notes)

## Main Points of Interest

1. Any usage of `@SpringBootTest` that uses both `args` and `properties` may need to be reconfigured due to new
   property source order for tests.

   [Ref-doc](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.7-Release-Notes#springboottest-property-source-precendence)
2. Any custom auto-config class currently defined in `spring.factories` should be moved to:
   `META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports`.
   Additionally, the new `@AutoConfiguration` annotations should be used in place of `@Configuration`.
   Note: This only includes `@Configuration` classes registered via `spring.factories`. This does not include
   classes like `edu.vt.middleware.boot.core.EnvSpringApplicationRunListener` as this is not an auto-config class.

   [Ref-doc](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.7-Release-Notes#new-autoconfiguration-annotation)
3. `WebSecurityConfigurerAdapter` has been deprecated and any usage should be updated to use `SecurityFilterChain`.

   [Ref-doc](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.7-Release-Notes#migrating-from-websecurityconfigureradapter-to-securityfilterchain)

   [Ref-impl](https://spring.io/blog/2022/02/21/spring-security-without-the-websecurityconfigureradapter)

