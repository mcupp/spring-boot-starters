# Upgrade from Spring Boot 2.6 to 3.x.x

## Docs
[Release Notes](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.0-Release-Notes)

[Spring Boot Migration](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.0-Migration-Guide)

[Spring Security Migration Guide](https://docs.spring.io/spring-security/reference/5.8/migration/index.html)

[Spring Framework Migration](https://github.com/spring-projects/spring-framework/wiki/Upgrading-to-Spring-Framework-6.x)

## Terms Used

To abbreviate the docs, the following abbreviations are used:
SB2 - Spring Boot Version 2.7.x
SB3 - Spring Boot Version 3.x.x
SS5 - Spring Security Version 5.8.x - Compatibility version
SS6 - Spring Security Version 6

## Main Points of Interest

1. Java 17 or later is required for SB3.
2. SS6 is required. If your app overrides any spring security dependencies, you must remove
   the overrides.
3. Java 16/17 implemented numerous changes but the most impactful is encapsulation of JDK internals, which can and
   likely will cause issues when you attempt to run your app. If your app, and to some extent dependencies, use any
   of these newly encapsulated internals, you will have to specify via JVM args which of the packages you need
   access. E.g. `--add-opens java.base/java.net=ALL-UNNAMED` gives access to the `java.net` packages.
   **NOTE:** Some encapsulated internals have alternative APIs, so you should attempt to move away from using
   the encapsulated modules. A complete list has been difficult to track down, but the following doc may be useful.
   [Docs](https://wiki.openjdk.org/display/JDK8/Java+Dependency+Analysis+Tool)


## Upgrade Steps

1. Update your app to ensure it is using at least SB2 `2.7.4`
2. Build your app and fix any current deprecation warnings emitted from Spring, at a minimum. You should attempt to
   resolve deprecation warnings from all dependencies. The console buffer is likely limited, so the easiest approach
   is to build and redirect output to a file and then grep/search that file for `deprecated`. E.g. `mvn clean
   package > build-output.txt`
3. SS6 has many changes and to assist with moving to this new version they have created a 5.8.x branch
   that has a majority of the SS6 changes but it is compatible with SB2. It also includes deprecation warnings on
   anything that will be removed in SS6. This will help ensure that all security changes have been implemented
   before trying to do the actual Spring Boot upgrade.
   * Temporarily update your SB2 project to use SS5 by setting the following in the `<properties>` section of your
     project pom. `<spring-security.version>5.8.1</spring-security.version>`
   * To the best of my knowledge we aren't using any of the following password encoders, but do a quick search just
     to verify. If you are using them, follow the docs
     [here](https://docs.spring.io/spring-security/reference/5.8/migration/index.html#_update_password_encoding) to
     update.
     * Pbkdf2PasswordEncoder
     * SCryptPasswordEncoder
     * Argon2PasswordEncoder
   * Again, I don't believe we use this anywhere, but search your app for usages of `Encryptors.queryableText` and
     follow the Spring Security migration guide linked at the top.
   * [Servlet Migrations](https://docs.spring.io/spring-security/reference/5.8/migration/servlet/index.html)
     * **Session management**. A majority of our apps disable sessions so this may not apply to anything other than ED-IAM.
       However, there are important changes and you should at least read about the changes.
       [Session Mgmt](https://docs.spring.io/spring-security/reference/5.8/migration/servlet/session-management.html)
     * **Exploit protection**. I believe all of our web apps disable CSRF protection, but again, this is worth skimming.
       [CSRF](https://docs.spring.io/spring-security/reference/5.8/migration/servlet/exploits.html)
     * **Important!!**
       [**Configuration**](https://docs.spring.io/spring-security/reference/5.8/migration/servlet/config.html)
       This is likely the most important step of the SS6 migration and will apply to most or all
       of our Spring apps. The main points I encountered are below, but please read through this section.
       * **SecurityFilterChain:** `antMatchers`, `mvcMatchers`, and `regexMatchers` from `authorizeHttpRequests` have
         been deprecated in favor of `requestMatchers`. This is to make configuration easier by not making you choose
         which `matcher` to use. Spring will decide based on the type of app; it will use a `mvcMatcher` if your app
         is using Spring MVC and will use `antMatcher` otherwise.
         Effectively, anywhere you have a definition like:
         ```java
         @Bean
         public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
           http
            .authorizeHttpRequests((authz) -> authz
              .antMatchers("/api/**")
              .anyRequest().authenticated()
            );
           return http.build();
         }
         ```
         Would become
         ```java
         @Bean
         public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
           http
            .authorizeHttpRequests((authz) -> authz
              .requestMatchers("/api/**") /* <--- This is all that changed */
              .anyRequest().authenticated()
            );
           return http.build();
         }
         ```
       * **WebSecurityCustomizer:** Similar changes to the previous step. See the "Servlet Migration" docs above.
       * **Important!!** **securityMatcher:** This is very important, especially if your app has more than one
         SecurityFilterChain beans. In our current apps, we are likely doing something like the following, which
         tells spring security which `RequestMatcher` to use for specific http requests. Without this, Spring will
         use the first matcher it encounters and that will cause issues.
         ```java
         @Bean
         public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
           http
            .requestMatchers(req -> req
              .mvcMatchers("/api/v1/**")
            )
            .authorizeHttpRequests((authz) -> authz
              .anyRequest().authenticated()
            );
           return http.build();
         }
         ```
         Would become
         ```java
         @Bean
         public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
           http
            .securityMatcher("/api/v1/**")
            .authorizeHttpRequests((authz) -> authz
              .anyRequest().authenticated()
            );
           return http.build();
         }
         ```
       * **Important!!** Spring no longer provides a `permitAll` filter chain by default and you must provide your
         own. It is also imperative that you provide an `@Order` to all of your `SecurityFilterChain` beans and the
         `permitAll` chain should have the lowest precedence (default for @Order). Your other `SecurityFilterChain(s)`
         should have an `@Order(precedence:int)` where `precedence` will be `1`, `2`, `3`, etc... The order of your
         other chains doesn't matter as long as they are different, unless you expect requests to propagate through
         each of your custom chains in a specific order.
       * If your app extends `WebSecurityConfigurerAdapter`, you must update to use the component based
         implementation. [Here](https://docs.spring.io/spring-security/reference/5.8/migration/servlet/config.html#_stop_using_websecurityconfigureradapter)
       * **Important!!** All of the `@Enable*` Spring Security annotations are no longer decorated with their own
         `@Configuration` annotation, so you must provide that yourself.
       * **GlobalMethodSecurityConfiguration** has been deprecated. If your app extends this class, you should
         remove it and reimplement any of the overridden config methods as Spring Beans. For instance, if you have
         something like the following:
         ```java
         class SecurityConfig extends GlobalMethodSecurityConfiguration {
           @Override
           protected MethodSecurityExpressionHandler createExpressionHandler() {
             return new MyExpressionHandler();
           }
         }
         ```
         Would become
         ```java
         class SecurityConfig {
           @Bean
           static MethodSecurityExpressionHandler createExpressionHandler() {
             return new MyExpressionHandler();
           }
         }
         ```
         Note that the new bean is `static`. This is necessary to ensure that Spring publishes it before it
         initializes Spring Security’s method security `@Configuration` classes.

         Important!! `@EnableGlobalMethodSecurity` has been deprecated in favor of more granular annotations like,
         `@EnableMethodSecurity`, but thus far I have been unable to get the custom
         `MethodSecurityExpressionHandler` to work correctly with this annotation. However,
         `@EnableGlobalMethodSecurity` has only been deprecated and Spring states that they will not be removing it
         in SS6, so continue to use it if you have similar issues.
       * There are additional sections for `Authorization`, `OAuth`, and `SAML` that should be reviewed if your app
         is using any of these security mechanisms. Those docs can be found in
         [this](https://docs.spring.io/spring-security/reference/5.8/migration/servlet/index.html) section of the docs.
   * Attempt to build your app again and again fix any deprecation warnings. The deprecation warning for
     `@EnableGlobalMethodSecurity` can be ignored if you had to continue using that annotation.
   * Hopefully you have now successfully upgraded your security implementation.
   * Remove the SS5 property you set earlier. `<spring-security.version>5.8.1</spring-security.version>`
4. Upgrade to the most recent 3.x `mw-spring-boot-starter-parent`.
5. Jakarta package names have changed and will prevent your app from building. The easiest way to update is by using
   the built-in tool in IntelliJ. [Instructions](https://blog.jetbrains.com/idea/2021/06/intellij-idea-eap-6/)
   Do note that you will likely have to fix import ordering after doing this to prevent checkstyle errors. The
   easiest method is to navigate to the files that changed as a result of this change, click "code" in the intellij
   menu bar, and then click "Optimize Imports".
6. Add the following to your pom (backend). This will report any application properties that are deprecated when you
   run the app. Be sure to remove this once you have updated those values.
   ```xml
   <dependency>
	  <groupId>org.springframework.boot</groupId>
	  <artifactId>spring-boot-properties-migrator</artifactId>
	  <scope>runtime</scope>
   </dependency>
   ```
7. Attempt to run your app. In general, the steps above should have fixed most of the issues.
8. Continue with the migration docs if any other issue arise.


